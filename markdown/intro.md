### Introduction


### Objectif

- Faire des pages web

----

<div style="display: inline-grid;grid-template-columns: 300px 300px  50px;grid-column-gap: 50px;">
	<div class="wrap">
		<iframe  class="scaledFrame" src="https://www.lequipe.fr" style='height:900px; width: 600px;'> </iframe>
	</div> 
	<div class="wrap">
		<iframe  class="scaledFrame" src="https://www.allocine.fr" style='height: 900px; width: 600px;'> </iframe> 
	</div>
</div>


### Enquête

- Qui a déjà fait une page web ?
	- Avec quel logiciel ?
	- Dans quel contexte ?

![](fig/fig1.jpg)


### Déroulement

- 1 CM/TD (en ligne sur Arche)
- 2 TD (en ligne sur Arche) -> coef 0.5
- 1 TP (en ligne sur Arche) -> coef 0.5

- Projet tuteuré pour les vacances de Noël (sujet en ligne sur Arche) -> **autre module**


### Lecture d’une page html

<div style="display: inline-grid;grid-template-columns: 300px 300px 300px 200px;">
	<div class="fragment fade-in-then-out" data-fragment-index="1">
		Différents fichiers<br>
		![](fig/fig10.png)
	</div>
	<div class="fragment fade-in-then-out" data-fragment-index="2">
		Navigateur<br>
		![](fig/fig11.png)
	</div>
	<div class="fragment fade-in-then-out" data-fragment-index="3">
		Page web<br>
		<div class="wrap">
		<iframe src="https://www.allocine.fr" style='height: 800px; width: 450px;' class="scaledFrame">
		</div>	
	</div>	
</div>