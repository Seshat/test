### Introduction


## Objective

- Design web pages

----

<div style="display: inline-grid;grid-template-columns: 300px 300px  50px;grid-column-gap: 50px;">
	<div class="wrap">
		<iframe  class="scaledFrame" src="https://www.lequipe.fr" style='height:900px; width: 600px;'> </iframe>
	</div> 
	<div class="wrap">
		<iframe  class="scaledFrame" src="https://www.allocine.fr" style='height: 900px; width: 600px;'> </iframe> 
	</div>
</div>


### Survey

- Who has already designed a web page?
	- Using which software?
	- In which context?

![](fig/fig1.jpg)


### Activities

- 1 Lecture/Tutorial classes (online on Arche)
- 2 Tutorial classes (online on Arche) -> coef 0.5
- 1 Practical Work (online on Arche) -> coef 0.5

- Supervised project for Christmas holidays (subject online on Arche) -> **other module** 


### Reading an HTML page

<div style="display: inline-grid;grid-template-columns: 300px 300px 300px 200px;">
	<div class="fragment fade-in-then-out" data-fragment-index="1">
		Different files<br>
		![](fig/fig10.png)
	</div>
	<div class="fragment fade-in-then-out" data-fragment-index="2">
		Browser<br>
		![](fig/fig11.png)
	</div>
	<div class="fragment fade-in-then-out" data-fragment-index="3">
		Web page<br>
		<div class="wrap">
		<iframe src="https://www.allocine.fr" style='height: 800px; width: 450px;' class="scaledFrame">
		</div>	
	</div>	
</div>