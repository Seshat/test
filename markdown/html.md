# Programmation HTML


### Traitements par le navigateur

- logiciels types:
	- bloc-note (**notepad++**, **sublime Text**, <a href="http://brackets.io" target="_blank">brackets</a>, ...)
	- Navigateur internet (**firefox**, **chrome**, **safari**, ...)

---
<div style="display: inline-grid;grid-template-columns: 500px 200px 200px;">
	<div>
		 bloc-note
		<code><pre>
			```
Bonjour le monde, comment vas-tu ? 
Au revoir !!!!!
```
		</pre></code>
	</div>	
	<div>
		Navigateur
	<iframe src="exemples/bonjour.html" style='height:175px; width: 100%'> </iframe>
	</div>
</div>


### Mise en page

- La mise en page va se faire par un langage informatique !
- C’est le HTML

<div style="width:50%;display: block;  margin: auto;">![](https://www.w3.org/html/logo/img/html5-topper.png)</div>


### La programmation HTML

- Le langage **HTML** (Hyper Text Markup Language) va de pair avec le protocole **HTTP** (Hyper Text Transport Protocol) qui permet de véhiculer des documents sur le Web
- HTML est le **langage universel** sur le réseau universel qu'est Internet


### La programmation HTML
- Un fichier HTML (extension **.htm** ou **.html**) **=** un fichier de type **texte**
- La création de documents HTML est à la portée de toutes les personnes qui savent se servir d'un simple éditeur de texte
- HTML n'est qu'un ensemble de commandes utilisant des balises intelligibles, (exemple : ```<title>```) 


### (petit) Historique
- Avant : Internet = gigantesque **réseau de serveurs**
- Années 90  : Tim Berners-Lee créé le **web**
- HTML 5: **2014**

<iframe src="https://en.wikipedia.org/wiki/HTML" style='height:600px; width: 100%;'> </iframe>


### Principe html5

- **Philosophie**  : séparation contenu, structure et présentation
- **Contenu** = tout ce qu'on porte à la connaissance de l'utilisateur via le langage naturel, les images, les sons et les animations
- **Structure** = organisation logique du contenu grâce à des éléments comme `<body>`, `<h1>`, `<p>`, etc. 
- **Présentation** = manière dont les contenus se manifestent pour l'utilisateur via un périphérique de sortie, comme un écran, une imprimante, un navigateur vocal, une plage braille, etc. 


### Idée supplémentaire d’HTML5

- Eviter «**Ctrl+C**», «**Ctrl+V**» !!!
 - Pas de mise en page, juste le contenu et la structure
 - En-tête à connaître par coeur
- Exemples :
```
HTML 4.01 Strict
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" 
	"http://www.w3.org/TR/html4/strict.dtd">
HTML 4.01 Transitional
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
HTML 4.01 Frameset
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
 "http://www.w3.org/TR/html4/frameset.dtd">
```


### Structure du document à spécifier

- le HTML5 définit un nouveau modèle d’organisation des contenus
- balises dédiées : `<header>`, `<article>`, `<nav>` ou encore `<footer>`
- Objectif : affecter un style (CSS) commun

![](https://www.w3schools.com/html/img_sem_elements.gif)


### Structure d'un document HTML
- **`<HTML>`**
	- Tout document HTML débute par la balise <HTML> qui signale que le document qui suit est codé en HTML
	- Exemple : `<HTML> document ... </HTML>`
- **`<HEAD>`**
	- La balise `<HEAD>` définit l'en-tête du document
	- très peu d'autres balises sont insérées
- **`<BODY>`**
	- Définit le corps du document (texte, balises, images, ...)
- **`<TITLE>`**
	- Titre unique qui doit être explicite car il est utilisé comme bookmark (marque page) par les interfaces de navigation


## La structure

Exemple :
```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> Mon premier document HTML </TITLE>
	</HEAD>
	<BODY>
   		... Texte du document ...
	</BODY>
</HTML>
```


### Résultat

```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> Mon premier document HTML </TITLE>
	</HEAD>
	<BODY>
   		Bonjour le monde, comment vas-tu ? 
		Au revoir !!!!!
	</BODY>
</HTML>
```

<iframe src="exemples/bonjour.html"  class='code' style='height:200px; width: 100%'> </iframe>


<iframe src="https://codesandbox.io/embed/1y1mz1o09j?fontsize=14&view=editor" title="Ex1" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>
<iframe src="https://codesandbox.io/embed/1y1mz1o09j?fontsize=14" title="ex1" style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>


## Les titres et paragraphes

- Il existe 6 niveaux de titres pour diviser un document

- Le chiffre indique le niveau d'intertitre, de **`H1`** à **`H6`**
	- Exemple : **`<H1> Grand titre (niveau 1)</H1`**>
    - ou : **`<H6> Petit titre (niveau 6) </H6>`**

- Chaque paragraphe débute par une balise ouvrante **`<P>`** et se termine par une balise fermante **`</P>`**

- L'insertion de commentaires dans un document est codée comme suit :
**`<!-- Ceci est un commentaire -->`**


```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> Document HTML </TITLE>
	</HEAD>
	<BODY>
		<h1>Politesse</h1>
		<P>Bonjour le monde, comment vas-tu ?</P>
		<P>Au revoir !!!!!</P>
	</BODY>
</HTML>
```

<iframe src="exemples/bonjour3.html"  class='code'  style='height:200px; width: 100%;'> </iframe>


## Les listes

- Il existe 4 principaux types de listes
	- Liste numérotée (**`<OL>`**)
	- Liste à puces ( **`<UL>`**)
	- Liste de type glossaire
	- Liste de type répertoire
- Chaque liste est délimitée par
	- **`<UL>`** et **`</UL>`** ou **`<DL>`** et **`</DL>`**
- Chaque élément de la liste a ses propres balises
	- **`<LI>`** pour la plupart des types de liste
	- **`<DT>`** et **`<DD>`** pour les listes de type glossaire
- Les listes peuvent être imbriquées


```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> Document HTML </TITLE>
	</HEAD>
	<BODY>
		<p>La Divine Comédie de Dante consiste en 3 livres :</p>
		<ul>
			<li>L'Enfer</li>
			<li>Le Purgatoire</li>
			<li>Le Paradis</li>
		</ul>
		<br>
		<ol>
			<li>L'Enfer</li>
			<li>Le Purgatoire</li>
			<li>Le Paradis</li>
		</ol>
	</BODY>
</HTML>
```
***Pas de liste dans des paragraphes (```<p>```)***


<iframe src="https://codesandbox.io/embed/1462kkkqll?fontsize=14&view=editor" title="Ex1" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>


### Les accents en HTML

- Enregistrement des documents HTML sur le serveur en code **ASCII**
	- plus de place pour les caractères accentués 
- On utilise donc des mots clés
	- Exemples :
		- à : **`&agrave;`** , &#224
		- ç : **`&ccdil;`** , &#231
		- é : **`&eacute;`** , &#233
		- è : **`&egrave;`** , &#232
		- ê : **`&ecirc;`** , &#234


## Les accents en HTML

- Mettre  **`<meta charset="utf-8">`** dans **`<head>`**

```
<!DOCTYPE html>
<HTML lang="fr">
	<HEAD>
		<TITLE> Document HTML </TITLE>
		<meta charset="utf-8">
	</HEAD>
	<BODY>
		<p>La Divine Comédie de Dante consiste en 3 livres :</p>
		...
	</BODY>
</HTML>
```
<iframe src="exemples/bonjour5.html"  class='code'   style='height:200px; width: 100%;'> </iframe>


```
...
<P>Liste de type glossaire :</P>
<DL>
     <DT> OLE <DD> Object Linking and Embedding, liaison et incorporation d'objets
     <DT> SQL <DD> Structured Query Language, langage de manipulation de données
</DL>
```

<iframe src="exemples/bonjour6.html"  class='code'   style='height:200px; width: 100%;'> </iframe>


## Les liens hypertexte

**`<A HREF="../menu.htm">`** | **`retour`** |  **`</A>`** 
--- || --- || ---
*Balise d'ouverture*       | *Texte*   |     *Balise fermante*    
----------     
- La balise de liaison est **`<A>`**
- Les liens hypertexte sont de 2 natures
	- absolus
	- relatifs
- Liens vers un document distant à l'aide de l'URL

	**`<A HREF="http://www.cern.ch">Page d'accueil du CERN</A>`**


## Les ancres	

- La balise **`<balise id="toto">`** définit une ancre
- Le  lien se fait avec **`<a href=”#toto”>`**
- Ca marche avec n'importe quelle balise 
- Une image peut être un déclencheur de lien


```
...
<H3>Lien absolu :</H3>
<A HREF="/Users/pierre-fredericvillard/Desktop/web/bonjour.html">
<H2 ALIGN=CENTER>Retour au premier document HTML</H2></A>
<H3>Lien relatif :</H3>
<A HREF="bonjour2.html">Retour au second document HTML</A>
<P>
<H3>Voici les deuxième, troisième et quatrième exemples :</H3>
<P>Aller au <A HREF="#quatrième">quatrième</A> exemple directement !</P>
<UL>
<LI>deuxième exemple</LI>
<LI><A HREF="bonjour4.html">troisième </A>exemple</LI>
<LI>...</LI>
<LI>...</LI>
<LI>...</LI>
<LI id="quatrième">vous êtes au quatrième exemple</LI>
```

<iframe src="exemples/bonjour7.html"  class='code'   style="height:200px; width: 100%" scrolling="yes"> </iframe>


## Les images
- Les images sont insérées par **`<IMG SRC="  " ALT=" ">`**
- Les dimensions exactes des images sont affichées
   - &rarr; optimise la vitesse de chargement 
```
<P><IMG SRC="map.gif" alt="carte des USA"></P>
```

 <iframe src="exemples/bonjour9.html" class='code' style="height:200px; width:600px" scrolling="yes"> </iframe>  


## Les styles de caractères

- **`<EM>`** L'emphase (texte en italique)
- **`<STRONG>`** Emphase "plus forte" (souvent en gras)
- **`<B>`** Texte en gras
- **`<CITE>`** Citation en italique
- **`<I>`** Texte en italique
- **`<CODE>`** Code source d'un fichier ou programme
- **`<SAMP>`** Texte d'exemple


```
<EM> A mettre en valeur </EM>
<STRONG>Encore plus fort</STRONG>
<P>Mettre en gras : <B>C'est en gras</B></P> 
<P>Le travail c'est la santé <CITE> Un illustre inconnu</CITE></P> 
<P> Texte en italique : <I> italique </I></P> 
<P><CODE>#include stdio.h</CODE></P> 
<HR>
<ADDRESS>E-mail : toto@toto.fr</ADDRESS>	
```

 <iframe src="exemples/bonjour11.html" class='code' style="height:200px; width:600px" scrolling="yes"> </iframe>


## Les tableaux

```
<TABLE>
	<TR>
		<TH>Légende</TH>
		<TD>Données</TD>
		<TD>Données</TD>
	</TR></TABLE>
<TABLE>
<CAPTION> ISI Première année</CAPTION>
	<TR>
		<TH>Nombre d'étudiants</TH>
		<TD>45</TD>
	</TR>
	<TR>
		<TH>Age moyen</TH>
		<TD>20 ans</TD>
	</TR>  
</TABLE>
```

<iframe src="exemples/bonjour12.html" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


- Les composantes d'un tableau
	- La légende : décrit le contenu du tableau
	- Les en-têtes : étiquettes de chaque série de données
	- Les données : informations présentées dans le tableau
	- Les cellules : les cases du tableau
- **`<TABLE>`** Délimite un tableau
- **`<CAPTION>`** Définit la légende
- **`<TR>`** Définit une ligne
- **`<TH>`** Définit les en-têtes
- **`<TD>`** Définit les données
- **`<TD></TD>`** Définit une cellule vide


### Les tableaux complexes
```
<TABLE>
	<TR>
		<TH ROWSPAN=2 COLSPAN=2></TH>
		<TH COLSPAN=2>Connexion</TH>
	   <TH ROWSPAN=2>Cavaliers</TH>   </TR>
	<TR>
		<TH>Carte mère</TH>
		<TH>Carte contrôleur</TH>   </TR>
	<TR>
		<TH ROWSPAN=2>Disque dur</TH>
		<TD>E-IDE</TD>
		<TD>Figure A</TD>
		<TD>Figure D</TD>
		<TD ROWSPAN=2>Figure G</TD>   </TR>
	<TR>
		<TD>SCSI</TD>
		<TD>Figure B</TD>
		<TD>Figure E</TD>   
	</TR>
</TABLE>
```

<iframe src="exemples/bonjour13.html" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


## Les formulaires

- **`<FORM>`** définit un formulaire

- **`<INPUT>`** indique qu'il s'agit d'un élément du formulaire

	- Attribut **`TYPE`** : 
		- "**`RADIO`**"(bouton rond)
		- "**`CHECKBOX`**"(case à cocher)
		- "**`TEXT`**"(chaîne de caractères)

	- Attribut **`NAME`** : indique le nom que l'on donne à l'élément

- **`<INPUT TYPE="SUBMIT" VALUE="VALIDER">`** définit le bouton de soumission


```
<form>
	<p>
		M. <input name=qualite type=radio value=mr> 
		Mme <input name=qualite type=radio value=mme> 
		Mlle <input name=qualite type=radio value=mlle>
	</p>
	<p>
		Votre nom : <input name=patronyme size=15> 
		Votre prénom : <input name=prenom size=30>
	</p>	
	<p>
		Votre mot de passe : <input name=clef type=password size=6> 
	</p>
	<p>
		Numéro : <input name=no type=int size=5>
		Rue <input name=lieu type=radio value=rue checked>
		Place <input name=lieu type=radio value=place>
		Avenue <input name=lieu type=radio value=ave>
		Boulevard <input name=lieu type=radio value=bvd>
	</p>
	<p>
		Adresse : <input name=adresse type=text size=40>
	</p>
	<p>
		Code postal : <input name=cp type=int size=5> 
		Ville : <input name=ville size=15>
	</p>
	<p>
		<input type=submit value=Valider> 
		<input type=reset value=Annuler>
	</p>
</form>
```

<iframe src="exemples/PROG15.HTM" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


```
<form>
	<p>
		Vous désirez une pizza contenant : 
		<select MULTIPLE name="composants" size="6">
			<option>Tomates
			<option selected>Olives
			<option>Oignons
			<option>Ail
			<option>Fines herbes
			<option>Parmesan
			<option>Capres
			<option>Poivrons
			<option>Chocolat
		</select>
	</p>
</form>
```

<iframe src="exemples/PROG16.HTM" class='code' style="height:120px; width:600px" scrolling="yes"> </iframe>


## Les balises "multimédias"

Nom de balise   |   description
-----|----
**`source`**   |  resource multimedia
**`audio`**   |  resource multimedia
**`video`**   |  resource multimedia
**`embed`**   |  resource multimedia   


### La balise source

- Permet de spécifier plusieurs alternatives de contenu multimédia
- Utilisée avec **`<video>`**, **`<audio>`** et **`<picture>`**
- Elle accepte les attributs suivants :
	- **`src`** : l'URL contenant la ressource.
	- **`type`** le type de média : 
	- Exemple : 

**`<source type="audio/ogg" src="song.ogg"/>`**


### La balise audio
- Elle permet de contrôler des flux audio.
- Elle accepte les attributs suivants :
	- **`src`** : l'URL contenant la ressource
	- **`autoplay`** : la lecture démarre dès l'ouverture
	- **`loop`** : la lecture se fait en boucle
	- **`preload`** : chargement  lancé à l'ouverture
	- **`controls`** : les contrôles seront présents
```
<audio controls>
	<source src="horse.ogg" type="audio/ogg">
	<source src="horse.mp3" type="audio/mpeg">
	Your browser does not support the audio element.
</audio>
```

<iframe src="exemples/bonjour14.html" class='code' style="height:50px; width:600px" scrolling="yes"> </iframe>


### La balise video 

- Afficher une vidéo sans plugins externes
- Elle accepte les attributs suivants :
	- **`src, autoplay, loop, preload, controls`** 
	- **`audio`** si positionné à mute enlève le son
	- **`width`** et **`height`** indique la taille de la video
	- **`poster`** indique une image représentant la vidéo.

```
<video width="640" height="480" preload 
  loop="loop" controls="controls" poster="logo.gif"> 
  <source src="monClip.h264" type="video/h264"/>
  <source src="monClip.mp4" type="video/mp4"/>
</video>
```

<iframe src="exemples/bonjour15.html" class='code' style="height:155px; width:600px" scrolling="yes"> </iframe>


### Attention au navigateur !

<iframe src="https://en.wikipedia.org/wiki/HTML5_video#Browser_support" class='code' style="height:600px; width:100%" scrolling="yes"> </iframe>